/*
 * player.hpp
 *
 *  Created on: Dec 6, 2014
 *      Author: jeroen
 */

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

class player
{
public:
	int pY, pX;

	player(){pY=0;pX=0;};
	void move(int fX, int fY);

private:
	int input;
};



#endif /* PLAYER_HPP_ */

/*
 * field.cpp
 *
 *  Created on: Dec 6, 2014
 *      Author: jeroen
 */
#include <iostream>
#include "field.hpp"

void field::printField(int x, int y, int pX, int pY)
{
	for(int i(0);i<x;i++)
	{
		for(int i2(0);i2<y;i2++)
		{
			if(i == pX && i2 == pY)
			{
				std::cout << "P";
			}else{
				std::cout << "*";
			}
		}
		std::cout << std::endl;
	}
}

void field::setFieldperim()//Set field size
{
	std::cout << "X(Default 10):";
	std::cin >> fieldX;

	std::cout << "Y(Default 10):";
	std::cin >> fieldY;
}

/*
 * player.cpp
 *
 *  Created on: Dec 6, 2014
 *      Author: jeroen
 */
#include <iostream>
#include "player.hpp"

void player::move(int fX, int fY)
{
	//Get input
	std::cout << "UP(1) DOWN(2) LEFT(3) RIGHT(4)\n";
	std::cout << "Move:";
	std::cin >> input;

	//Boundary checking && Processing
	if(input == 1)//UP
	{
		if(pY>0){pY-=1;}
	}
	else if(input == 2)//DOWN
	{
		if(pY<fY-1){pY+=1;}
	}
	else if(input == 3)//LEFT
	{
		if(pX>0){pX-=1;}
	}
	else if(input == 4)//RIGHT
	{
		if(pX<fX-1){pX+=1;}
	}
}



